# Korean translation for simple-ccsm
# Copyright (C) 2008 launchpad.net/compiz
# This file is distributed under the same license as the simple-ccsm package.
#
msgid ""
msgstr ""
"Project-Id-Version: simple-ccsm\n"
"Report-Msgid-Bugs-To: https://gitlab.com/compiz/simple-ccsm/issues\n"
"POT-Creation-Date: 2017-12-09 01:20+0300\n"
"PO-Revision-Date: 2008-07-25 05:19+0200\n"
"Last-Translator: Lee June Hee <bugbear5@gmail.com>\n"
"Language-Team: Korean <ko@li.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Minimal"
msgstr ""

msgid "Medium"
msgstr ""

msgid "Advanced"
msgstr ""

msgid "Ultimate"
msgstr ""

msgid "Provides a simple desktop environment with very few effects."
msgstr ""

msgid ""
"Provides good balance between attractiveness and moderate performance "
"requirements."
msgstr ""

msgid "Provides more aesthetically pleasing set of effects."
msgstr ""

msgid ""
"Provides very advanced and eye-catching set of effects. Requires faster "
"graphics card."
msgstr ""

msgid "Enabled"
msgstr "사용 가능"

msgid "Disabled"
msgstr "사용 불가능"

msgid "Zoom"
msgstr "확대"

msgid "Colorfilter"
msgstr "색깔 필터"

msgid "None"
msgstr ""

msgid "Can't find the animation plugin."
msgstr ""

msgid "Can't find any animation extension plugins."
msgstr ""

#, c-format
msgid "%s (Cover)"
msgstr "%s (커버)"

#, c-format
msgid "%s (Flip)"
msgstr "%s (플립)"

msgid "Cover"
msgstr "커버"

msgid "Flip"
msgstr "플립"

msgid ""
"Desktop effects are not supported on your current hardware / configuration. "
"Would you like to cancel enabling of desktop effects or run them anyway?"
msgstr ""

msgid "_Cancel"
msgstr ""

msgid "Default"
msgstr "기본"

#, fuzzy
msgid "Simple CompizConfig Settings Manager"
msgstr "간단한 Compiz 설정 관리자"

msgid "_Enable desktop effects"
msgstr ""

#, fuzzy
msgid "Profile:"
msgstr "프로필:"

msgid "<b>Description</b>"
msgstr "<b>설명</b>"

msgid "<b>Animations</b>"
msgstr "<b>애니메이션</b>"

msgid "<b>Effects</b>"
msgstr "<b>효과</b>"

msgid "<b>Desktop</b>"
msgstr "<b>데스크탑</b>"

msgid "<b>Accessibility</b>"
msgstr "<b>접근성</b>"

msgid "Info"
msgstr "정보"

msgid "Enable animations"
msgstr "애니메이션 켜기"

#, fuzzy
msgid "Enable extra animations"
msgstr "애니메이션 켜기"

msgid "<b>Open window</b>"
msgstr "<b>창 열기</b>"

msgid "<b>Close Window</b>"
msgstr "<b>창 닫기</b>"

#, fuzzy
msgid "<b>Focus window</b>"
msgstr "<b>창 닫기</b>"

msgid "<b>Minimize window</b>"
msgstr "<b>창 최소화</b>"

msgid "Animations"
msgstr "애니메이션"

msgid "<b>Switcher</b>"
msgstr "<b>스위처</b>"

msgid "Deformation:"
msgstr ""

msgid "Opacity:"
msgstr ""

#, fuzzy
msgid "Enable Reflection"
msgstr "애니메이션 켜기"

#, fuzzy
msgid "Enable 3D Windows"
msgstr "출렁이는 창 켜기"

#, fuzzy
msgid "<b>Cube Effects</b>"
msgstr "<b>효과</b>"

msgid "Enable Scale"
msgstr "스케일 켜기"

msgid "Enable Expo"
msgstr "엑스포 켜기"

msgid "Enable Blur"
msgstr "흐림 효과 켜기"

msgid "Enable Wobbly"
msgstr "출렁이는 창 켜기"

msgid "<b>Additions</b>"
msgstr "<b>추가 기능</b>"

msgid "Effects"
msgstr "효과"

msgid "<b>Appearance</b>"
msgstr "<b>모양</b>"

msgid "<b>Desktop columns</b>"
msgstr "<b>데스크탑 가로 갯수</b>"

msgid "<b>Desktop rows</b>"
msgstr "<b>데스크탑 세로 갯수</b>"

msgid "<b>Desktop preview</b>"
msgstr "<b>데스크탑 미리보기</b>"

msgid "Desktop"
msgstr "데스크탑"

msgid "Screen zoom"
msgstr "화면 확대"

msgid "Area zoom"
msgstr "영역 확대"

msgid "<b>Enable zoom</b>"
msgstr "<b>확대 가능</b>"

msgid "<b>Screen zoom</b>"
msgstr "<b>화면 확대</b>"

msgid "<b>Area zoom</b>"
msgstr "<b>영역 확대</b>"

msgid "Accessibility"
msgstr "접근성"

msgid "<b>Screen Edges</b>"
msgstr "<b>화면 모서리</b>"

msgid "Edges"
msgstr "모서리"

msgid "Configure Compiz with CompizConfig"
msgstr ""
